import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push } from '@ionic-native/push';
import { HomePage } from '../pages/home/home';
// declare var cordova: any;
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public push: Push) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      // this.pushSetup();
    });
  }

  // initPushwoosh() {
  //   // alert('Cordova is ' + JSON.stringify(cordova))
  //   var pushwoosh = cordova.require("pushwoosh-cordova-plugin.PushNotification");
  //   alert('Pushwoosh is ' + JSON.stringify(pushwoosh));
  //   // Should be called before pushwoosh.onDeviceReady
  //   document.addEventListener('push-notification', function (event) {
  //     alert(event);
  //     let message = (event as any).notification.message; // Push message
  //     let userData = (event as any).notification.userdata; // Custom push data

  //     if (userData) {
  //     // handle custom push data here
  //       alert('user data: ' + JSON.stringify(userData));
  //     }

  //     // var notification = event.notification;
  //     // handle push open here
  //   });

  //   // Initialize Pushwoosh. This will trigger all pending push notifications on start.
  //   pushwoosh.onDeviceReady({ projectid: "302029267851", appid : "268F5-5AD3E" });

  //   //register for pushes
  //   pushwoosh.registerDevice(
  //     function (status) {
  //       var pushToken = status;
  //       // alert(pushToken);
  //       alert('push token: ' + JSON.stringify(pushToken));
  //       // this.pushSetup();
  //     },
  //     function (status) {
  //       alert(JSON.stringify(['failed to register ', status]));
  //     }
  // );
  // }


  // pushSetup() {
  //   const options: PushOptions = {
  //     android: {
  //       // senderID: '302029267851'
  //     },
  //     ios: {
  //       alert: 'true',
  //       badge: true,
  //       sound: 'false'
  //     },
  //     windows: {},
  //     browser: {
  //       pushServiceURL: 'http://push.api.phonegap.com/v1/push'
  //     }
  //   };

  //   const pushObject: PushObject = this.push.init(options);

  //   pushObject.on('notification').subscribe((notification: any) => alert('Received a  notification' + JSON.stringify(notification)));

  //   pushObject.on('registration').subscribe((registration: any) => alert('Device registered' + registration));

  //   pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error + ' and message is' + error.message));
  // }
}


import { Component } from '@angular/core';
import { NavController, Platform, Events, AlertController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Network } from '@ionic-native/network';
declare var cordova: any;
declare var universalLinks: any;
declare var device: any;
declare var navigator: any;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private browser: any;
  private connected: boolean = true;
  private loading: any;
  private showInitialConfirmBox: any;
  private check1: boolean = false;
  private check2: boolean = false;
  private check3: boolean = false;
  /**
   * Constructor.
   * @param navCtrl Navigation controls instance
   * @param platform the platform instance
   * @param iab the iab instance
   */
  constructor(public navCtrl: NavController, public platform: Platform, private iab: InAppBrowser, private events: Events, private network: Network, private alertCtrl: AlertController) {
    // this.presentLoadingDefault();
    this.platform.ready().then(() => {
      this.showInitialConfirmBox = (window.localStorage.getItem('skip') == null || window.localStorage.getItem('skip') == undefined) ? true : false;
      // navigator.notification.activityStart("Please Wait", "Loading.....");          
      this.connected = (this.checkConnection() == 'none' || this.checkConnection() == null || this.checkConnection() == 'null' || this.checkConnection() == 'undefined') ? false : true;
      var options = { dimBackground: true };
      if (this.connected) {
        this.kickStartPushwoosh();
        if(!this.showInitialConfirmBox) {
          this.kickStartIab();
        }
      }
    });
  }

  kickStartPushwoosh() {
    if (this.platform.is('ios') || this.platform.is('android')) {
      // alert("PushwooshService init: Running on push compatible platform " + this.platform.userAgent() + ')');
      this.initPushwoosh();
      // this.platform.registerBackButtonAction(() => {
      //   alert('back pressed!');
      // }, 10)
      // document.addEventListener('backbutton', () => {
      //   alert('Back button tapped');
      // }, false);

      // this.events.subscribe('backbutton', this.backPressed);    
    } else {
      // alert("PushwooshService init: No compatible platform available.  Skipping init.)");
      return;
    }
  }

  /**
   * Launching the In App Browser.
   */
  kickStartIab() {
    // alert('the uuid is ' + device.uuid);
    let value = window.localStorage.getItem("email_address");
    if(window.localStorage.getItem('skip') == 'true') {
      // alert('opening navigation page....');
      this.openIab('https://info.mobileiron.com/Navigation.html?regId=' + device.uuid + '&skip=true', true);            
    // alert('Opening this url:');
    // alert('https://info.mobileiron.com/AppDev_Navigation.html?mobileApp=true&regId=' + device.uuid + '&skip=true');
    } else {
      if(value && value != 'null' && value != 'undefined') {
        // alert("https://info.mobileiron.com/AppDev_RegistrationForm.html?mobileApp=true&regId='" + device.uuid + "&email=" + value);              
        this.openIab('https://info.mobileiron.com/Registration-LP.html?regId=' + device.uuid + '&email=' + value, true);
      // alert('Opening this url:');
      // alert('https://info.mobileiron.com/AppDev_RegistrationForm.html?mobileApp=true&regId=' + device.uuid + '&email=' + value);
        window.localStorage.removeItem('email_address');
      }  else {
        // alert("https://info.mobileiron.com/AppDev_RegistrationForm.html?mobileApp=true&regId='" + device.uuid);        
        this.openIab('https://info.mobileiron.com/Registration-LP.html?regId=' + device.uuid, true);      
    //  alert('Opening this url:');
    //  alert('https://info.mobileiron.com/AppDev_RegistrationForm.html?mobileApp=true&regId=' + device.uuid);
      }
    }
    
    // if (value) {
    //   this.openIab('https://info.mobileiron.com/AppDev_RegistrationForm.html?mobileApp=true&regId=' + device.uuid + '&email=' + value + skipString, true);
    //   window.localStorage.removeItem('email_address');
    // } else {
    //   this.openIab('https://info.mobileiron.com/AppDev_RegistrationForm.html?mobileApp=true&regId=' + device.uuid + skipString, true);
    // }
  }

  checkConnection() {
    return this.network.type;
  }

  retryConnection() {
    this.connected = this.checkConnection() == 'none' ? false : true;
    if (this.connected) {
      this.kickStartPushwoosh();
      if(window.localStorage.getItem('skip')) {
        this.kickStartIab();
      }
    }
  }

  // backPressed() {
  //   alert('back is pressed!');
  //   // this.platform.exitApp();    
  // }

  /**
   * Initialize PushWoosh.
   */
  initPushwoosh() {
    var pushwoosh = cordova.require("pushwoosh-cordova-plugin.PushNotification");
    pushwoosh.setMultiNotificationMode();
    // Should be called before pushwoosh.onDeviceReady
    document.addEventListener('push-notification', (event) => {
      // alert(JSON.stringify(event));
      let message = (event as any).notification.message; // Push message
      let userData = (event as any).notification.userdata; // Custom push data
      if (userData) {
        // handle custom push data here
        // alert('user data: ' + JSON.stringify(userData.url));
        this.openIab(userData.url, userData.internal);
      }
    });

    // Initialize Pushwoosh. This will trigger all pending push notifications on start.
    pushwoosh.onDeviceReady({ projectid: "718664034167", appid: "268F5-5AD3E" });

    //register for pushes
    pushwoosh.registerDevice(
      function (status) {
        var pushToken = status;
        // alert('push token: ' + JSON.stringify(pushToken));
      },
      function (status) {
        // alert(JSON.stringify(['failed to register ', status]));
      }
    );

    // universalLinks.subscribe('ul_myExampleEvent', (eventData) => {
    //   // alert('Did launch application from the link: ' + JSON.stringify(eventData));
    //   this.openNavigation();
    // });
  }

  /**
   * Open url in iab.
   * @param url url to be opnened.
   */
  openIab(url: string, internal: boolean) {
    // alert('opening ' + url);
    // let options : InAppBrowserOptions = {
    //     location : 'no',//Or 'no' 
    //     hidden : 'no', //Or  'yes'
    //     clearcache : 'yes',
    //     clearsessioncache : 'yes',
    //     zoom : 'yes',//Android only ,shows browser zoom controls 
    //     hardwareback : 'no',
    //     mediaPlaybackRequiresUserAction : 'no',
    //     shouldPauseOnSuspend : 'no', //Android only 
    //     closebuttoncaption : 'Close', //iOS only
    //     disallowoverscroll : 'no', //iOS only 
    //     toolbar : 'no', //iOS only 
    //     enableViewportScale : 'no', //iOS only 
    //     allowInlineMediaPlayback : 'no',//iOS only 
    //     presentationstyle : 'pagesheet',//iOS only 
    //     fullscreen : 'yes',//Windows only    
    // };

    if (internal) {
      var win = window.open(url, "_blank", 'location=no,clearsessioncache=yes,toolbar=no');

      win.addEventListener('loadstart', (event: any) => {
        // alert('start');
        navigator.notification.activityStart("Please Wait", "Loading...");
      })
      win.addEventListener('loadstop', (event: any) => {
        // alert('stop');        
        navigator.notification.activityStop();
      })
      // this.browser = this.iab.create(url, "_blank", options);
    } else {
      window.open(url, '_system', 'location=yes');
      return false;
    }
  }

  /**
   * Open Iab after skip.
   */
  openGeneralIab() {
    localStorage.setItem('skip', 'true');
    this.kickStartIab();
  }

  /**
   * Register now button pressed.
   */
  register() {
    if(!this.check1 || !this.check2 || !this.check3) {
      let alert = this.alertCtrl.create({
        title: 'Unable to proceed.',
        subTitle: 'You must select all the checkboxes to register.',
        buttons: ['Cancel']
      });
      alert.present();
    } else {
      window.localStorage.setItem("skip", "false");
      this.kickStartIab();
    }
  }
}
